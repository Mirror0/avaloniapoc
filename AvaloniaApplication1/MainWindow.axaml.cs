using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.Devices.Enumeration;
using System.Collections.Generic;

namespace AvaloniaApplication1
{
    public partial class MainWindow : Window
    {
        private RfcommServiceProvider _provider;
        private StreamSocket _socket;
        private ListBox _listBox;
        private Button _connectBtn;
        private Label _label;
        public MainWindow()
        {
            AvaloniaXamlLoader.Load(this);
            //InitializeComponent();
            Init();
        }

        public void Init()
        {
            Button button = this.FindControl<Button>("Btn");
            _listBox = this.FindControl<ListBox>("DList");
            _connectBtn = this.FindControl<Button>("Connect");
            _label = this.FindControl<Label>("status");
            button.Click += Button_Click;
            _listBox.SelectionChanged += _listBox_SelectionChanged;
            _connectBtn.Click += _connectBtn_Click;
        }

        private void _connectBtn_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            DeviceInformation device = _listBox.SelectedItem as DeviceInformation;
            _label.Content = device.Name;
        }

        private void _listBox_SelectionChanged(object? sender, SelectionChangedEventArgs e)
        {
            _connectBtn.IsEnabled = true;
        }

        private async void Button_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            //DeviceInformationCollection devices =  await DeviceInformation.FindAllAsync(BluetoothDevice.GetDeviceSelector());
            DeviceInformationCollection devices =  await DeviceInformation.FindAllAsync();
            string content = devices.Count.ToString();
            _listBox.Items = devices;
            _label.Content = content;
        }

        private async void InitializeComponent()
        {
            _provider = await RfcommServiceProvider.CreateAsync(RfcommServiceId.ObexObjectPush);

            StreamSocketListener listener = new StreamSocketListener();
            listener.ConnectionReceived += Listener_ConnectionReceived;
            await listener.BindServiceNameAsync(_provider.ServiceId.AsString(), SocketProtectionLevel.BluetoothEncryptionAllowNullAuthentication);
            DataWriter writer = new DataWriter();
            writer.WriteByte(0x0A);
            writer.WriteInt32(200);
            IBuffer data = writer.DetachBuffer();
            _provider.SdpRawAttributes.Add(0x0300,data);

            _provider.StartAdvertising(listener);
        }

        private void Listener_ConnectionReceived(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            _provider.StopAdvertising();
            sender.Dispose();
            _socket = args.Socket;
            Label label = this.FindControl<Label>("status");
            label.Content = "Connected";
        }
    }
}
